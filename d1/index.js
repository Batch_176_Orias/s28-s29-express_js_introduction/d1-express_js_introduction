//goal is to create a server-side app using express web framework

//relate the task to smtg that one do on a daily basis

/*
append the entire app to node package manager
package.json -> the 'heart' of every node project. Also contains different metadata that describes the structure of the project

scripts -> used to declare and describe custom commands and keyword that can be used to execute this project with correct runtime env

`start` is globally recognize amongst code projects and frameworks as the `default` command script to execute a task/project
however for *unconventional* keywords or command, it has to append the command `run`
npm run <custom command>

*/

/*
1. Identify and prepare the ingredients.
*/
// let express = require("express");

//express => will be used as the main component to create the server
//need to be able to gather the utilities and components needed that the express library will provide.
//require() -> directive used to get the library needed inside the module

//create a runtime environment that automatically fix all the changes in the app

//have to use a utility called nodemon
//upon starting the entry point module with nodemon you will be able to `append` the application w proper run time environment. Allowing to save time and effort upon commiting changes to app
/*console.log(`This will be the server`);
console.log(`NEW CHANGES`);
console.log(`Let's goooo`);
console.log(`Supppp`);
console.log(`Hello world`);
*/
//Possible to even insert items like text art into run time environment

console.log(`
Welcome to Express API Server
────────────█───────────────█
────────────██─────────────██
─────────────███████████████
────────────█████████████████
───────────███████████████████
──────────████──█████████──████
─────────███████████████████████
────────█████████████████████████
────────█████████████████████████
───███──▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒──███
──█████─█████████████████████████─█████
──█████─████████████████──███████─█████
──█████─██████────────█──█────███─█████
──█████─█████─▓▓▓▓▓▓▓█──█▓▓─▓─███─█████
──█████─███─█─▓▓▓▓▓▓█──█▓▓─▓▓─███─█████
──█████─██──█─▓▓▓▓▓█──█▓▓─▓▓▓─███─█████
──█████─███─█─▓▓▓▓█──█▓▓─▓▓▓▓─███─█████
──█████─█████────█──█─────────███─█████
──█████─█████████──██████████████─█████
───███──████████──███████████████──███
────────█████████████████████████
─────────███████████████████████
──────────█████████████████████
─────────────██████───██████
─────────────██████───██████
─────────────██████───██████
─────────────██████───██████
──────────────████─────████
`);